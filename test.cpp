#include "tessellator.hpp"
#include <cstdio>

int main(){
    CHWTessellator tess;
    tess.Init(PIPE_TESSELLATOR_PARTITIONING_INTEGER, PIPE_TESSELLATOR_OUTPUT_TRIANGLE_CW);
    tess.TessellateTriDomain(1.0, 2.0, 3.0, 4.0);
    int pts = tess.GetPointCount();
    int idxs = tess.GetIndexCount();
    DOMAIN_POINT *points = tess.GetPoints();
    int *indices = tess.GetIndices();
    for (unsigned i = 0; i < pts; ++i) {
        printf("%f, %f\n", points[i].u, points[i].v);
    }
    printf("---\n");
    for (unsigned i = 0; i < idxs; ++i) {
        printf("%u\n", indices[i]);
    }

    return 0;
}
